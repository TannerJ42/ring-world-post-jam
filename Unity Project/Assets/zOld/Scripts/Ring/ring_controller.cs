﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ring_controller : MonoBehaviour
{
    static bool randomSet = false;
    public static bool rotateLeft;

    [HideInInspector]
    public float surfaceRadius;

    [HideInInspector]
    public ring_spawner spawner;

    public Transform surfaceTransform;
 

    public GameObject nodePrefab;



    public GameObject onDestroyParticles;
    public float onDestroyParticlesTime = 3f;

    private float currentSize;
    public float startingSize = 0.1f;
    public float maxSize = 5f;
    public float scaleSpeed = 0.1f;
    private float rotationSpeed = 0f;
    [HideInInspector]
    public Vector3 rotationDirection;
    public float minRotationSpeed = 0.01f;
    public float maxRotationSpeed = 0.1f;

    private List<NodeController> nodes;
    public int nodesSpawned = 0;
    public int nodesSpawnedIndex;

    public bool isOuterRing = false;

    // Use this for initialization
    void Start()
    {
        if (!randomSet)
        {
            randomSet = true;
            rotateLeft = Random.Range(0, 2) == 1 ? true : false; 
        }

        rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);

        if (rotateLeft)
        {
            rotationDirection = Vector3.back;
        }
        else
            rotationDirection = Vector3.forward;
        rotateLeft = !rotateLeft;

        surfaceRadius = Vector2.Distance(transform.position, surfaceTransform.position);

        nodes = new List<NodeController>();
    }
	
    public void onNodeDestroyed(NodeController node)
    {
        nodes.Remove(node);

        if (nodes.Count == 0)
            OnRingDestroyed();
    }

	// Update is called once per frame
	void Update()
    {
        surfaceRadius = Vector2.Distance(transform.position, surfaceTransform.position);

        if (nodesSpawnedIndex > 0 &&
            currentSize >= spawner.sizes[nodesSpawnedIndex])
        {

            for (int i = 0; i <= spawner.numOfNodes[nodesSpawnedIndex] - 1; i++)
                CreateNode();

            nodesSpawnedIndex--;
        }
    }

    void CreateNode()
    {
        if (nodePrefab == null)
            return;
        nodesSpawned++;
        GameObject obj = (GameObject)Instantiate(nodePrefab);
        NodeController cont = obj.GetComponentInChildren<NodeController>();
        cont.controller = this;
        nodes.Add(cont);
        obj.transform.Rotate(Vector3.forward * Random.Range(0, 36) * 10);
        RotateAroundRing rot = obj.GetComponentInChildren<RotateAroundRing>();
        if (rot != null)
            rot.SetRing(this);
    }

    void FixedUpdate()
    {

        transform.Rotate(rotationDirection * rotationSpeed * Time.fixedDeltaTime);
    }

    public void OnRingDestroyed()
    {
        if (onDestroyParticles != null)
        {
            GameObject explosion = (GameObject)Instantiate(onDestroyParticles, transform.position, transform.rotation);
            Vector3 scale = explosion.transform.localScale;
            scale.x = transform.localScale.x * 2;
            scale.y = transform.localScale.y * 2;

            
            explosion.transform.localScale = scale;
            Destroy(explosion, onDestroyParticlesTime);
        }

        for (int i = 0; i <= nodes.Count - 1; i++)
        {
            Destroy(nodes[i].gameObject);
        }

        spawner.OnRingDestroyed();

        Destroy(gameObject);
    }

    public NodeController[] getNodes()
    {
        if (nodes == null)
            return null;
        NodeController[] n = nodes.ToArray();
        return n;
    }
}
