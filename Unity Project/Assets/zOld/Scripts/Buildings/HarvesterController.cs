﻿using UnityEngine;
using System.Collections;

public class HarvesterController : MonoBehaviour
{
    public float timeBetweenHarvests = 1f;
    public int energyPerHarvest = 1;

	// Use this for initialization
	void Start() 
	{
        InvokeRepeating("GrantEnergy", timeBetweenHarvests, timeBetweenHarvests);
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}

    void GrantEnergy()
    {
        ResourceManager.instance.energy += energyPerHarvest;
    }
}
