﻿using UnityEngine;
using System.Collections;

public class HouseController : MonoBehaviour
{

    public float timeBetweenGeneratePeople = 1f;

    public int initialPeople = 2;
    public int repeatPeople = 0;

	// Use this for initialization
	void Start() 
	{
        ResourceManager.instance.people += initialPeople;
        InvokeRepeating("GrantPeople", timeBetweenGeneratePeople, timeBetweenGeneratePeople);
    }
	
	// Update is called once per frame
	void Update() 
	{
		
	}

    void GrantPeople()
    {
        ResourceManager.instance.people += repeatPeople;
    }
}
