﻿using UnityEngine;
using System.Collections;
using System;

public class yuufo_controller : MonoBehaviour
{
    public ring_spawner ringSpawner;
    public ring_controller targetRing;
    public NodeController targetNode;
    public CircleCollider2D attackDistance;

    public float minHoverHeight = 0.2f;
    public float maxHoverHeight = 1f;

    public float maxHoverLeft = -1f;
    public float maxHoverRight = 1f;

    private Vector3 direction;

    private State state = State.Entering;

    private bool hoverUp = false;
    public float hoverSpeed = 3f;

    private bool hoverRight = false;

    public NodeController.NodeTypes[] targetOrder;

    public GameObject weaponParticle;

	// Use this for initialization
	void Start() 
	{
        ringSpawner = GameObject.FindGameObjectWithTag("Ring Spawner").GetComponent<ring_spawner>();

        direction = Vector3.forward;
    }
	
    void Hover()
    {
        float vertSpeed = hoverSpeed;
        float horizSpeed = hoverSpeed;
        if (!hoverUp)
        {
            vertSpeed *= -1;
        }
        if (!hoverRight)
        {
            horizSpeed *= -1;
        }
        Vector3 position = transform.localPosition;
        position.y += vertSpeed * Time.fixedDeltaTime;
        position.x += horizSpeed * Time.fixedDeltaTime;
        transform.localPosition = position;
    }

    void AcquireTargets()
    {
        if (targetRing == null)
            return;

        NodeController[] nodes = targetRing.getNodes();

        if (nodes == null)
            return;

        float dist = 1000;
        int index = 1000;

        for (var i = 0; i < nodes.Length - 1; i++)
        {
            float d = Vector2.Distance(transform.position, nodes[i].transform.position);
            int enemyIndex = Array.IndexOf(nodes, nodes[i].nodeType);

            if (enemyIndex < index || (enemyIndex == index && d < dist))
            {
                dist = d;
                index = Array.IndexOf(nodes, nodes[i].nodeType);
                targetNode = nodes[i];
            }
        }

        if (targetNode != null)
            state = State.Attacking;
        else
            state = State.Pathing;

    }

    void FixedUpdate()
    {
        if (targetRing == null || targetNode == null)
        {
            weaponParticle.SetActive(false);
        }

        if (targetRing == null || targetRing.gameObject == null)
            targetRing = ringSpawner.getOuterRing();



        switch (state)
        {
            case State.Entering:
                if (targetRing != null)
                {
                    Vector3 pos = transform.localPosition;
                    pos.y -= 1 * Time.fixedDeltaTime;
                    //pos.y = Mathf.Max(pos.y, targetRing.surfaceRadius + maxHoverHeight);

                    if (pos.y < targetRing.surfaceRadius + maxHoverHeight)
                        state = State.Pathing;

                    transform.localPosition = pos;
                }
                break;
            case State.Attacking:
                if (targetNode != null)
                {
                    // find out which way to rotate
                    Vector3 dir = targetNode.transform.parent.eulerAngles;

                    //Debug.Log(dir);
                    
                    // rotate in that direction
                    transform.parent.Rotate(dir * hoverSpeed * 1 * Time.fixedDeltaTime);

                    float dist = Vector2.Distance(targetNode.transform.position, transform.position);

                    if (dist < attackDistance.radius)
                    {
                        // face target

                        Vector3 targetDir = targetNode.gameObject.transform.position - transform.position;

                        float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
                        angle += 90;
                        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

                        // fire weapon
                        if (weaponParticle != null)
                        {
                            weaponParticle.SetActive(true);

                            
                            float current = weaponParticle.GetComponent<Renderer>().bounds.size.x;

                            Vector3 size = weaponParticle.transform.localScale;
                            size.x = dist * size.x / current;

                            //size.x = Vector2.Distance(targetNode.transform.position, transform.position);
                            weaponParticle.transform.localScale = size;

                            //if (!weaponParticle.isPlaying)
                            //{
                            //    weaponParticle.gameObject.transform.parent.gameObject.SetActive(true);
                            //    Vector3 size = weaponParticle.gameObject.transform.parent.localScale;
                            //    size.x = Vector2.Distance(transform.position, targetNode.transform.position);
                            //    weaponParticle.gameObject.transform.parent.localScale = size;
                            //    weaponParticle.Play();
                            //}

                        }
                    }
                    else
                    {

                        weaponParticle.SetActive(false);
                        FaceCenter();
                        //weaponParticle.Stop();
                    }

                    AcquireTargets();


                }
                else
                {
                    FaceCenter();
                }
                break;
            case State.Pathing:
                AcquireTargets();
                //Hover();
                break;
        }

        if (targetRing != null)
        {
            Vector3 pos = transform.localPosition;

            if (pos.y < targetRing.surfaceRadius + minHoverHeight)
            {
                pos.y = targetRing.surfaceRadius + minHoverHeight;
                hoverUp = true;
            }
            if (pos.y > targetRing.surfaceRadius + maxHoverHeight)
            {
                pos.y = targetRing.surfaceRadius + maxHoverHeight;
                hoverUp = false;
            }

            if (pos.x < maxHoverLeft)
            {
                pos.x = maxHoverLeft;
                hoverRight = true;
            }
            if (pos.x > maxHoverRight)
            {
                pos.x = maxHoverRight;
                hoverRight = false;
            }

            transform.localPosition = pos;
        }
    }

    private void FaceCenter()
    {
        Vector3 targetDir = transform.parent.transform.position - transform.position;
        float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
        angle += 90;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    enum State
    {
        Entering,
        Pathing,
        Attacking
    }
}
