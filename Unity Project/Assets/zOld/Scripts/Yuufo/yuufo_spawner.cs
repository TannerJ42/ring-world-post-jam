﻿using UnityEngine;
using System.Collections;

public class yuufo_spawner : MonoBehaviour
{
    public GameObject yuufoPrefab;

    public int[] spawnTimes;
    int spawnTimesIndex = 0;

    public bool keepSpawning = true;

	// Use this for initialization
	void Start() 
	{
        //InvokeRepeating("SpawnYuufo", 3, 3);

        Invoke("SpawnYuufo", spawnTimes[spawnTimesIndex]);
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}

    void SpawnYuufo()
    {
        if (keepSpawning)
        {

            GameObject yuufo = (GameObject)Instantiate(yuufoPrefab);
            Vector3 yuufoPosition = yuufo.transform.GetChild(0).position;
            yuufoPosition.y = 50f;
            yuufo.transform.GetChild(0).position = yuufoPosition;
            yuufo.transform.Rotate(Vector3.forward * Random.Range(0, 359));

            if (++spawnTimesIndex > spawnTimes.Length - 1)
                spawnTimesIndex = spawnTimes.Length - 1;

            Invoke("SpawnYuufo", spawnTimes[spawnTimesIndex]);
        }
    }
}
