﻿using UnityEngine;
using System.Collections;

public class ShopController : MonoBehaviour
{
    public NodeController currentNode;

    private Collider2D[] colliders;

    public AudioClip clip;
    AudioSource source;

    void Start()
    {
        source = gameObject.AddComponent<AudioSource>();
        source.clip = clip;
        colliders = GetComponentsInChildren<Collider2D>();
    }

    public void OnShow()
    {
        if (source != null)
            source.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider == null) //|| (hit.collider.gameObject != this.gameObject))
            {
                if (currentNode != null)
                {
                    currentNode.EnableCollider();
                    currentNode = null;
                }
            }
            else if (System.Array.IndexOf(colliders, hit.collider) == -1)
            {
                if (currentNode != null)
                {
                    currentNode.EnableCollider();
                    currentNode = null;
                }
            }
        }

        if (currentNode == null || currentNode.gameObject == null)
            gameObject.SetActive(false);
    }

    public void AttemptPurchase(NodeController.NodeTypes type)
    {
        if (currentNode == null || currentNode.gameObject == null)
            return;

        bool success = currentNode.BuildStructure(type);

        if (success)
            currentNode = null;
    }
}
