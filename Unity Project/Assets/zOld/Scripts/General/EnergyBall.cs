﻿using UnityEngine;
using System.Collections;

public class EnergyBall : MonoBehaviour
{
    public int energyPerClick = 1;

    public AudioClip energyTap;
    public AudioSource energySource;

    // Use this for initialization
    void Start()
    {
        energySource = GetComponent<AudioSource>();
        energySource.clip = energyTap;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {

        ResourceManager.instance.energy += energyPerClick;
        energySource.Stop();
        energySource.Play();
    }
}
