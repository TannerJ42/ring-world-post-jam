﻿using UnityEngine;
using System.Collections;

public class GameplayMusic : MonoBehaviour
{
    AudioClip[] clips;

    public AudioClip intro;
    public AudioClip ring4;
    public AudioClip ring3;
    public AudioClip ring2;
    public AudioClip ring1;

    public AudioSource source;

    private ring_spawner spawner;

    // Use this for initialization
    void Start() 
	{
        source = gameObject.AddComponent<AudioSource>();
        spawner = GameObject.FindGameObjectWithTag("Ring Spawner").GetComponent<ring_spawner>();
        source.clip = intro;
        source.Play();
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (!source.isPlaying)
        {
            int count = spawner.GetRingCount();
            switch (count)
            {
                case 0:
                    break;
                case 1:
                    source.clip = ring1;
                    break;
                case 2:
                    source.clip = ring2;
                    break;
                case 3:
                    source.clip = ring3;
                    break;
                case 4:
                    source.clip = ring4;
                    break;
            }

            if (source.clip != null)
                source.Play();
        }
	}
}
