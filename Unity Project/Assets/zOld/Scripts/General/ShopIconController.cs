﻿using UnityEngine;
using System.Collections;

public class ShopIconController : MonoBehaviour
{
    ShopController shop;

    public NodeController.NodeTypes type;

    public GameObject iconCover;

    TextMesh energy;
    TextMesh people;

    public TextMesh pop;
    public TextMesh energ;

    bool textSet = false;

	// Use this for initialization
	void Start() 
	{
        shop = GetComponentInParent<ShopController>();

        if (shop == null)
            throw new System.Exception("Unable to find ShopController for ShopIconController");

        if (iconCover == null)
            throw new System.Exception("Unable to find iconCover for ShopIconController");



    }
	
    public void Refresh()
    {
        if (!ResourceManager.instance.CanPurchase(type))
            iconCover.SetActive(true);
        else
            iconCover.SetActive(false);
    }

	// Update is called once per frame
	void Update() 
	{
        if (!textSet)
        {
            textSet = true;
            int energy = ResourceManager.instance.GetEnergyCost(type);
            int people = ResourceManager.instance.GetPeopleCost(type);

            pop.text = people.ToString();
            energ.text = energy.ToString();
        }

        Refresh();
	}

    void OnMouseDown()
    {
        shop.AttemptPurchase(type);
    }
}
