﻿using UnityEngine;
using System.Collections;

public class DumbRingController : MonoBehaviour
{
    bool moving = false;
	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
        Invoke("GoAway", 1f);
	}

    void FixedUpdate()
    {
        if (moving)
        {
            Vector3 pos = transform.position;
            pos.x -= 0.1f;
            transform.position = pos;
        }
    }

    void GoAway()
    {
        moving = true;
        Destroy(this.gameObject, 3f);
    }
}
