﻿using UnityEngine;
using System.Collections;

public class GrantResource : MonoBehaviour
{
    public ResourceType type;

    public enum ResourceType

    {
        Energy,
        People
    }

	// Use this for initialization
	void Start() 
	{
		
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}

    void OnMouseDown()
    {
        switch (type)
        {
            case ResourceType.Energy:
                ResourceManager.instance.energy += 1;
                break;
            case ResourceType.People:
                ResourceManager.instance.people += 1;
                break;
        }
    }
}
