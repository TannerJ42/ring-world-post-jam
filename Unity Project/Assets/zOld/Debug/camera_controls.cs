﻿using UnityEngine;
using System.Collections;

public class camera_controls : MonoBehaviour
{
    public float minimumZoom = 1;
    public float maximumZoom = 10;
    public float zoomSpeed = 0.3f;

    public float dragSpeed = 2f;
    private Vector3 oldPos;

    Vector3 dragOrigin = Vector3.zero;

	void Update()
    {
        // movement
        checkForMovement();

        // zoom
        float scroll = Input.GetAxis("Mouse ScrollWheel");
	    if (scroll > 0) // out
        {
            Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize - zoomSpeed, minimumZoom);
        }
        else if (scroll < 0) // in
        {
            Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize + zoomSpeed, maximumZoom);
        }
	}

    private void checkForMovement()
    {
        if (Input.GetMouseButtonDown(0))
        {
            oldPos = transform.position;
            dragOrigin = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition) - dragOrigin;
            transform.position = oldPos + -pos * dragSpeed;
        }
    }
}
