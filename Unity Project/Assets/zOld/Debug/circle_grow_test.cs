﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class circle_grow_test : MonoBehaviour
{
    public float startingSize = .2f;
    public float growthRate = 1.01f;
    public float maxGrowth = 3.0f;

    public float rotationSpeed = 0.005f;

    public SpriteRenderer image;

    List<Transform> children;

    // Use this for initialization
    void Start ()
    {
        Vector3 scale = transform.localScale;
        scale.x = startingSize;
        scale.y *= startingSize;
        transform.localScale = scale;

        children = new List<Transform>();

        for (int i = 0; i <= transform.parent.childCount - 1; i++)
        {
            if (transform.parent.GetChild(i).gameObject != this.gameObject)
                children.Add(transform.parent.GetChild(i));
        }
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (rotationSpeed != 0)
            transform.Rotate(Vector3.forward * -rotationSpeed);

        if (transform.localScale.x < maxGrowth)
        {
            Vector3 scale = transform.localScale;
            scale.x *= growthRate;
            scale.y *= growthRate;
            transform.localScale = scale;
        }
        return;
	}
}
