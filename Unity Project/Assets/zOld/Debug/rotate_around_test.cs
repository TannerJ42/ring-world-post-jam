﻿using UnityEngine;
using System.Collections;

public class rotate_around_test : MonoBehaviour {
    public GameObject target;
    Quaternion lastRotation;

	// Update is called once per frame
	void Update()
    {
        if (target == null)
            return;

        float radius = Vector2.Distance(target.transform.position, target.transform.parent.transform.position);
        Vector3 position = transform.position;
        position.y = radius;
        transform.position = position;
    }

    void FixedUpdate()
    {
        if (target == null)
            return;

        float newRotation = Quaternion.Angle(lastRotation, target.transform.parent.transform.rotation);
        lastRotation = target.transform.parent.transform.rotation;
    }
}
