﻿using UnityEngine;
using System.Collections;

public class Circle_Spawner : MonoBehaviour
{
    int numObjects = 12;
    public GameObject prefab;
    public float CRadius = 2;
    void Start()
    {
        Vector3 center = transform.position;
        for (int i = 0; i < numObjects; i++)
        {
            int a = i * 30;
            Vector3 pos = RandomCircle(center, CRadius, a);
            Instantiate(prefab, pos, Quaternion.identity);
        }
    }
    void Update()
    {
         CRadius = transform.position.x;
         Vector3 newPos = new Vector3(CRadius, CRadius, CRadius);
         prefab.transform.position = newPos;

    } 
        Vector3 RandomCircle(Vector3 center, float radius, int a)
    {
        Debug.Log(a);
        float ang = a;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }
}